#!/bin/bash -x
# This is old - may not work with newer versions


# Git installation
version=2.4.3 

# Check existing version and leave if already installed:
echo "Checking current version..."
currentVersion  = `git version`
if [[ $currentVersion =~ "$version" ]];
then 
	echo "Version $version is already installed. Exiting"
	exit 0
fi

# Check arch type:
uname=`uname -a`
echo #*******************************************************#
if [[ $uname =~ "Ubuntu" ]];
then
	echo "This will install Git v$version on your ***Ubuntu*** machine"
	uninstall="apt-get -y remove git"
	insatller="apt-get update; apt-get -y install libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev autoconf-archive libssl-dev libcurl4-openssl-dev expat libexpat1-dev lib64expat1-dev autoconf"
else
	echo "This will install Git v$version on your ***RHEL*** machine"	
	uninstall="yum -y remove git"
	insatller="yum update; yum -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel"
fi

echo "First, removing old version..."
$uninstall
echo "Done\r\n\r\n"
$installer

cd /tmp
# wget https://github.com/git/git/archive/v$version.tar.gz
cp /products/EE/DevOps/Progs/SCM/Git/git-$version.tar.gz .
tar -xzf git-$version.tar.gz

cd git-$version
make configure
./configure --prefix=/usr
make all
make install
git --version
cd /tmp

#cleanup
rm -rf git-$version
rm -rf $version.tar.gz   